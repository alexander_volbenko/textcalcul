package com.example.textcalcul;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Entity;

public class MainActivity extends AppCompatActivity {
    EditText firstOperand;
    EditText secondOperand;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        firstOperand = (EditText) findViewById(R.id.editTextFirst);
        secondOperand = (EditText) findViewById(R.id.editTextSecond);
    }

    public void sumButton(View view) {
        int num1, num2, result;
        try {
            num1 = Integer.parseInt(firstOperand.getText().toString());
            num2 = Integer.parseInt(secondOperand.getText().toString());
            result = num1 + num2;
            setContentView(R.layout.activity_result);
            TextView textView = (TextView) findViewById(R.id.result);
            if (num2 < 0) {
                textView.setText(num1 + " + (" + num2 + ") = " + result);
            } else {
                textView.setText(num1 + " + " + num2 + " = " + result);
            }
        } catch (NumberFormatException e){
            setContentView(R.layout.activity_result);
            TextView textView = (TextView) findViewById(R.id.result);
            textView.setText("Неправильные переменные");
        }
    }
}
